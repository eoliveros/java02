package deloitte.ccademy.lesson02.array;

import java.util.logging.Logger;
import java.util.logging.Level;
/**
 * This class find a key word inside a String 
 * @author Emiliano Oliveros Rodriguez
 */
public class PalabraClave {
	private static final Logger LOGGER = Logger.getLogger(TrueFalse.class.getName());

	String resultado = "Palabra no encontrada";

	public String palabraEncontrada(String[] directorio, String palabraClave) {

		try {
			for (int x = 0; x < directorio.length; x++) {
				if (palabraClave == directorio[x]) {
					resultado = "Palabra Clave Encontrada";
				}
			}
			LOGGER.info("PALABRA ENCONTRADA");
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "ERROR", ex);

		}
		return resultado;
	}
}
