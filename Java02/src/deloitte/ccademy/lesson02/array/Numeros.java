package deloitte.ccademy.lesson02.array;

import java.util.logging.Logger;
import java.util.logging.Level;

/**
 * This class obtain through different methods an array with the biggest, the smallest and
 * repeated numbers.
 * @author Emiliano Oliveros Rodriguez
 *
 */

public class Numeros {
	private static final Logger LOGGER = Logger.getLogger(Numeros.class.getName());

	/**
	 * This method evaluates which number of the array is the biggest one.
	 * @param array of numbers
	 * @return int number
	 */
	public static int mayor(int[] numeros) {
		int mayor = Integer.MIN_VALUE;

		try {

			for (int i = 0; i < numeros.length; i++) {
				mayor = Integer.max(mayor, numeros[i]);
			}
			LOGGER.info("El mayor es: " + mayor);
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "ERROR");
		}
		return mayor;
	}

	/**
	 * This method evaluates which number of the array is the smallest one. 
	 * @param array of numbers
	 * @return int number
	 */
	public static int menor(int[] numeros) {
		int menor = Integer.MAX_VALUE;

		try {

			for (int i = 0; i < numeros.length; i++) {
				menor = Integer.min(menor, numeros[i]);
			}
			LOGGER.info("El menor es: " + menor);
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "ERROR");
		}
		return menor;
	}
}
