package deloitte.ccademy.lesson02.array;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This method separate values of "true" and "false" into two different lists
 * these both included in a principal single list.
 * @author Emiliano Oliveros Rodriguez
 *
 */
public class TrueFalse {
	private static final Logger LOGGER = Logger.getLogger(TrueFalse.class.getName());

	public ArrayList<Object> sortBoolean(ArrayList<Boolean> arrBoolean) {

		ArrayList<Boolean> arrTrues = new ArrayList<Boolean>();
		ArrayList<Boolean> arrFalses = new ArrayList<Boolean>();
		ArrayList<Object> arrObject = new ArrayList<Object>();
		try {
			for (Boolean boolean1 : arrBoolean) {
				if (boolean1 == true) {
					arrTrues.add(true);
				} else {
					arrFalses.add(false);
				}

			}
			LOGGER.info("Se han ordenado correctamente");
			arrObject.add(arrTrues);
			arrObject.add(arrFalses);
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "ERROR" + e);
		}
		return arrObject;

	}

}
