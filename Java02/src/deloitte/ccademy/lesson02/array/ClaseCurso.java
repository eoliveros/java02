package deloitte.ccademy.lesson02.array;

import java.util.ArrayList;
import java.util.Collections;
/**
 * This class use the attributes of aggregate,remove and sort to return an array
 * with names and dates with this changes.
 * @author Emiliano Oliveros Rodriguez
 *
 */
public class ClaseCurso implements Comparable<ClaseCurso>{
	public String nombre;
	public String fecha;
	public ClaseCurso() {
		// TODO Auto-generated constructor stub
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	
	/**
	 * This method aggregate the attributes name and date to the list and then
	 * return the list with the changes.
	 * @param an array list
	 * @return an array of type clase curso.
	 */
	
	

	}
	/**
	 * This method remove the attributes name and date from the list and then
	 * return the list with the changes.
	 * @param an array list
	 * @return an array of type clase curso.
	 */
	public static ArrayList<ClaseCurso> remover(ArrayList<ClaseCurso> cursos, ClaseCurso curso) {
		ArrayList<ClaseCurso> claseCursos = new ArrayList<ClaseCurso>();

		claseCursos = cursos;
		for(ClaseCurso elemento: cursos) {
			if(elemento.equals(curso)){
				claseCursos.remove(curso);
				break;
			}
		}
		

		return claseCursos;

}
	/**
	 * This method compare the atribute name using an override function 
	 * from the list which compare the names and then return an ordered list.
	 * @param an array list
	 * @return an array of type clase curso.
	 */
	
	@Override
	public int compareTo(ClaseCurso e) {
		if(getNombre() == null || e.getNombre() == null) {
			return 0;
			
		}
		return getNombre().compareTo(e.getNombre());
	}
}



